Title: dev-libs/pyinotify has been moved to dev-python/pyinotify
Author: Kylie McClain <somasis@exherbo.org>
Content-Type: text/plain
Posted: 2015-05-14
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-libs/pyinotify

dev-libs/pyinotify was moved to dev-python/pyinotify, since I think it's a more
fitting category for it.

Install dev-python/pyinotify, then uninstall dev-libs/pyinotify:

1. Take note of anything depending on it

    cave resolve \!dev-libs/pyinotify

2. Install dev-python/pyinotify

    cave resolve -x dev-python/pyinotify

3. Reinstall anything mentioned in step 1. (you probably need to `cave sync`)

4. Uninstall dev-libs/pyinotify.

    cave resolve -x \!dev-libs/pyinotify

If anything breaks, you keep the pieces; make sure to do it in this order.

