ASIO SDK

You need to agree to our license agreement if you want to download the ASIO SDK:

§ 1 Object of the Agreement 

1. The object of this agreement consists of the Steinberg ASIO (Audio Stream I/O) Software Development Kit version 2.3, respectively any preview version of this Kit, comprising of documentation, example code, and several ASIO examples. These are described hereinafter as the "Licensed Software Developer Kit". 

2. In case a preview or beta version of the Licensed Software Developer Kitis provided, the Licensee is allowed to use the Licensed Software Developer Kitsolely for internal evaluations. Any publishing, distribution or transfer to a third party is not permitted. 

3. Steinberg is the holder of all copyrights, rights of ownership, and other rights concerning the Licensed Software Developer Kit. 

4. The Licensed Software Developer Kitcontains information about 
• how to develop an ASIO Driver, and 
• how to extend an application so that it can host ASIO Drivers, that are 
developed under the ASIO Specification, either by Steinberg or any Third-Parties. Currently the Licensed Software Developer Kit is running on the following computer platforms: 
- Windows XP, Windows Vista, Windows 7 and Windows8 (32 and 64bit) 

 

§ 2 Granting of Rights 

1. Steinberg hereby grants to the Licensee a non-exclusive, worldwide, nontransferable license during the term of this agreement to use the ASIO Interface Technology solely: 

a) for the development of ASIO Drivers and/or for the development of an application that can host ASIO Drivers (herein ASIODriver Compliant Product), 

b) publish, sell or otherwise distribute a ASIO Driver Compliant Product under his own brand name that is using parts or all of the Licensed Software Developer Kit. 

2. In case the Licensee receives a preliminary version of the Licensed Software Developer Kit, the Licensee is not allowed to publish any ASIO Driver Compliant Product to the public using a preliminary Software Developer Kit. The Licensee is only allowed to publish a ASIO Driver Compliant Product to the public based on a Licensed Software Developer Kitwhich is not declared as a preliminary version by Steinberg. 

3. The Licensee has no permission to sell, license, give-away and/or distribute the Licensed Software Developer Kitor parts of it for the use as software developer kit in any way, for example, integrated in any framework application, on any medium, including the Internet, to any other person, including sub-licensors of the Licensee or companies where the Licensee has any involvement. This includes re-working this specification, or reverse-engineering any products based upon this specification. 

4. In case the Licensed Software Developer Kitis modified, it shall not be published, sold or distributed without agreement of Steinberg. Furthermore, the naming of the Licensed Software Developer Kit shall not include “ASIO 2.3 SDK” or any combination containing the VST brand without permission of Steinberg.

5. In case the Licensed Software Developer Kitis extended by additional code, it shall not be used in any other description than “(Licensees company name) extension to the Steinberg ASIO 2.3 SDK”. 

6. The Licensee recognizes the value of the goodwill associated with the mark ASIO Technology and acknowledges that such goodwill exclusively belongs to the benefit of Steinberg and belongs to Steinberg. The Licensee warrants that it will not use the mark ASIO Technology on promotional merchandise, with the exception of demo versions of any of his product making use of this SDK. The Licensee warrants that it will not use the mark ASIO Technology on or in connection with products obscene, pornographic, excessively violent, or otherwise in poor taste. 

7. If the Licensee is developing a ASIO Driver Compliant Product, that is using parts or all of the Licensed Software Developer Kit, and this product is not published under his own name but will be published under the name of a third party, this third party has to agree to be bound by Sections 2.1 to 2.3 and 3 of this ASIO Driver SDK Licensing Agreement. The third party has to completely complywith these provisions. If the third party is not in accordance with these conditions, the third party is not allowed to distribute this product which is using parts or allof the Licensed Software Developer Kit. 

8. If the Licensee is planning to publish a ASIO Driver Compliant Product, under his own name or under the name of a third party, that is using parts or all of the Licensed Software Developer Kit, the Licensee is under the obligation to inform Steinberg about it by sending the signed ‘Steinberg ASIO SDK Licensing Agreement’ to Steinberg, either by mail, or by fax. 

 

§ 3 Use of Trademarks 

1. If the Licensee is publishing a ASIO Driver Compliant Product under his own name that is using parts or all of the Licensed Software Developer Kit, the Licensee shall be under an obligation to refer to Steinberg’s copyrights and trademarks in the following way: 

a) In case that the Licensee is publishing a “boxed product”, the Licensee shall display in a visible manner the ASIO Logo and Steinberg’s copyright notice on the packages. Steinberg’s Copyrights notice:”ASIO is a trademark and software of Steinberg Media Technologies GmbH” 

b) In case the ASIO Driver Compliant Product is published without a physical package (e.g. download), the Licensee shall display the ASIO Logo and Steinberg’s copyrights notice on the Licensee’s website in the context of the ASIO Driver Compliant Product. 

c) The Licensee shall include the ASIO Logo and Steinberg’s copyrights notice in 
• all electronic documentation, regardless of the media used, such as PDF manuals, websites etc. and • all printed and electronic advertising materials. 
The ASIO Logo artwork and usage guidelines are part of the "Licensed Software Developer Kit" and are supplied by Steinberg in digital format. 

d) In the ‘about box’ or an alternative place (e.g.help menu, startup screen) of the product in one of the following formats: 
• ASIO Driver Technology by Steinberg Media Technologies, 
• ASIO Driver Interface Technology by Steinberg Media Technologies GmbH. 

 

§ 4 Fees and Royalties 

1. This license is non-royalty bearing and the Licensee shall not be obligated to pay to Steinberg any fees or royalties with respect to theASIO Interface Technology. 

 

§ 5 Limitation of Liability 

1. Subject to the provisions in the following sub-sections, Steinberg shall only be liable, irrespective of the legal grounds, for damages caused by the intentional or grossly negligent conduct of Steinberg, its legal representatives, managerial employees or any other vicarious agents. In the case of damage caused by the grossly negligent conduct of any other vicarious agents, the liability shall be limited to those damages which must typically be expected within the scope of an agreement such as the present one. Any further liability other than as permitted under this agreement shall be excluded. 

2. Any liability of Steinberg for damages arising from violation of life, body and health, from the assumption of a guarantee or from a procurementrisk as well as Steinberg's liability for damages pursuant to the Product Liability Act (Produkthaftungsgesetz) shall remain unaffected. 

3. To the extent the liability of Steinberg is excluded pursuant to the subsections of these provisions, this shall also apply to the benefit ofSteinberg's employees in the event the Licensee files any claims directly against them. 

§ 6 Product Warranty 

1. Steinberg licences the ASIO Interface Technologyon an "AS IS" basis. Steinberg makes no warranties, express or implied, including without limitation the implied warranties of merchantability and fitness for a particular purpose, regarding the ASIO Interface Technology or operation and use in combination with the Licensee’s program. Neither the Licensee, its employees, agents or Distributors have any right to make any other representation, warranty or promise with respect to the ASIO Interface Technology. 

2. In no event shall Steinberg be liable for incidental, indirect or consequential damages arising from the use, or distribution of the ASIO Interface Technology by the Licensee, whether theory of contract, product liability or otherwise. All claims for indemnification for losses by the Licensee itself or by third parties shall be excluded. 

3. Steinberg may in its sole discretion discontinue the distribution of the current ASIO Driver SDK and/or release improved versions of the Licensed Software Developer Kitbut offers no commitment whatsoever those releases will occur at anytime or for anybody. 

 

§ 7 Infringement 

1. Steinberg represents and warrants that, as of the date of this Agreement, it is not aware of any claim or action alleging that ASIO, the ASIOInterface Technology, or the content of the Licensed Software Developer Kitinfringes any third party intellectual property right. 

2. Steinberg, however, disclaims any obligation of defense or indemnify of the Licensee or its customer with respect to any such claim or action, or otherwise arising out of this agreement. Steinberg shall have no liability arising out of any such actual or alleged intellectual property infringement. 

3. The Licensee, however, shall promptly notify Steinberg, in writing, of each such infringement claim of which the Licensee becomes aware. Steinberg may defend the Licensee against such claims. In such case, the Licensee is obligated to duly support Steinberg’s defense. 

 

§ 8 Relationship between the two Parties 

1. Nothing stated in this Agreement will be construed as creating the relationships of joint ventures, partners, principal, agent or whatsoever.The Licensee shall not be entitled to represent Steinberg or to make statements on its behalf. 

 

§ 9 Term and Termination 

1. The Agreement shall run for an unlimited period.

2. If the Licensee is in breach of any material obligations set out in this agreement and does not cure such breach by Steinberg’s written demand within 14 days, Steinberg shall be entitled to terminate this agreement immediately in writing and inform the Licensee verbally about it. In such a case, this license and all the rights granted to the Licensee herein shall immediately cease. 

3. In case that Steinberg publishes a new version of the Software Developer Kitaccording to a separate licensing agreement, Steinberg is entitled to terminate this Agreement and any foregoing ASIO Driver SDK Licensing Agreement regarding previous versions of the ASIO Driver SDK with a 6 months written notice. For the validity of the termination it shall be sufficient that Steinberg sends the termination to the last known email address of the Licensee. 

4. The right to extraordinary termination for goodcause shall remain unaffected.

 

§ 10 Final Provisions 

1. The Licensee declares himself to be in agreement with the use of any personal data obtained through this licensing relationship by Steinberg for its own company purposes, and for the purposes of its Company Group, within the meaning of the relevant Data Protection laws. 

2. This Agreement is the complete and exclusive understanding between the parties with respect to the subject matter hereof. Amendments, supplements and notices of termination of this Agreement must be made in writing. The rescission of this Agreement or an alteration to the requirement of the written form must also be made in writing. 

3. If any one stipulation of this Agreement should beor become invalid, completely or in part, this shall not affect the validity of the remaining stipulations. The invalid stipulations shall be deemed to be replaced with a valid regulation which comes as closely as commercially desired possible to the purpose originally intended for the ineffective provision; the same shall apply in the case or a contractual gap. 

4. Any and all prior ASIO Driver SDK agreements between Steinberg and the Licensee regarding previous ASIO Driver SDK versions shall be automatically terminated by signing this Agreement. 

5. This agreement and the interpretation thereof shall be exclusively subject to the laws of the Federal Republic of Germany without the application of the UN Convention on the Sale of Goods (CISG). Place of jurisdiction for alldisputes is Hamburg. In addition, each party can bring an action against the other party at the general place of jurisdiction of the other party. 

6. This agreement does not require a specific form (e.g. recording by a notary).

 

