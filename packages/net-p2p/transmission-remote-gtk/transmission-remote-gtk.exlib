# Copyright 2014-2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

ever is_scm && require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 ] ]
require github [ release=${PV} suffix=tar.xz ]
require freedesktop-desktop

SUMMARY="GTK remote control for the Transmission BitTorrent client"
SLOT="0"
LICENCES="GPL-2"

MYOPTIONS="
    appindicator [[ description = [ Use AppIndicator for tray icon rather than a standard tray icon ] ]]
    geoip [[ description = [ Display which country peers are from in the peer list ] ]]
    libnotify [[ description = [ Show notifications when torrents are added or finish ] ]]
    proxy [[ description =  [ Allow usage of proxies for connecting to the daemon ] ]]
    rss [[ description = [ Add torrents to the remote server from RSS feeds ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        core/json-glib[>=0.8]
        dev-libs/glib:2[>=2.44]
        net-misc/curl
        x11-libs/gtk+:3[>=3.16]
        appindicator? ( dev-libs/libappindicator )
        geoip? ( net-libs/GeoIP )
        libnotify? ( x11-libs/libnotify )
        proxy? ( net-libs/libproxy )
        rss? ( net-libs/libmrss )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--disable-desktop-database-update'
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'appindicator libappindicator'
    'geoip libgeoip'
    'libnotify libnotify'
    'proxy libproxy'
    'rss libmrss'
)

# sandbox violations due to dbus usage, using test-dbus-daemon causes more violations trying to use /dev files...
RESTRICT=test

