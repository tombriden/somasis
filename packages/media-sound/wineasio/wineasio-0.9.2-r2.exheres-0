# Copyright 2014-2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
#
# Based in part on 'https://aur.archlinux.org/packages/wi/wineasio/PKGBUILD' which
# is assumed to be copyrighted, as no licensing or clear statement of copyright
# exists on the Arch User Repository's site. Relevant lines from PKGBUILD follow:
#     Maintainer: Joakim Hernberg <jhernberg@alchemy.lu>
#     Contributor: Ray Rashif <schiv@archlinux.org>
#     Contributor: Shinlun Hsieh <yngwiexx@yahoo.com.tw>
#

require sourceforge [ suffix=tar.gz ]

SUMMARY="ASIO to JACK driver for Wine"
SLOT="0"
LICENCES="LGPL-2"

PLATFORMS="~amd64"

DEPENDENCIES="
    build+run:
        app-emulation/wine
        media-libs/steinberg-asio
        media-sound/jack-audio-connection-kit
"

WORK="${WORKBASE}/${PN}"

src_prepare() {
    edo cp "${ROOT}"/usr/$(exhost --target)/include/steinberg-asio/asio.h ./asio.h
    for file in Makefile Makefile64;do
        edo sed -e 's/^CEXTRA.*-m32 /CEXTRA =/'                         \
                -e "s#/usr/include#/usr/$(exhost --target)/include/#"   \
                -e "s#^PREFIX.*#PREFIX=/usr/$(exhost --target)#"        \
                -e "s#^CC =.*#CC=$(exhost --tool-prefix)cc#"            \
                -e "s#^CXX =.*#CXX=$(exhost --tool-prefix)c++#"         \
                -i ${file}
    done
    if [[ $(exhost --target) == x86_64-* ]]; then
        edo ./prepare_64bit_asio
    fi
}

src_install() {
    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/lib/wine
    edo install -Dm755 "${PN}.dll.so" "${IMAGE}"/usr/$(exhost --target)/lib/wine/${PN}.dll.so
}

pkg_postinst() {
    elog "To register the dll's driver in the default Wine prefix, run \`regsvr32 wineasio\`."
}
