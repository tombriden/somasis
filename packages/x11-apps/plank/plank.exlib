# Copyright 2014-2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require vala [ vala_dep=true ]
require gtk-icon-cache
require gsettings

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="The simplest dock on the planet, providing just what a dock needs and absolutely nothing more"

SLOT="0"
LICENCES="GPL-3"

HOMEPAGE="https://launchpad.net/${PN}"

BUGS_TO="somasis@exherbo.org"

MYOPTIONS="debug doc"

DEPENDENCIES="
    build:
        dev-libs/libxml2
        dev-util/intltool[>=0.35.0]
        virtual/pkg-config
    build+run:
        base/libgee:=[>=0.5.2][gobject-introspection]
        dev-libs/glib:2[>=2.40.0]
        dev-libs/libdbusmenu:0.4[gobject-introspection]
        gnome-desktop/libwnck:3.0[gobject-introspection]
        x11-libs/bamf[>=0.2.92][gobject-introspection]
        x11-libs/cairo[>=1.13] [[ note = [ it really requires 1.10, but HiDPI support is used with 1.13 ] ]]
        x11-libs/gdk-pixbuf[>=2.26.0][gobject-introspection]
        x11-libs/gtk+:3[>=3.10.0][gobject-introspection]
        x11-libs/libXfixes[>=5.0]
        x11-libs/libXi[>=1.6.99]
"

# Plank's tests use Xorg stuff which is a no-no
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-barriers
    --enable-dbusmenu
    --disable-apport
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    'debug benchmark'
    'doc docs'
)

DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-headless-tests --disable-headless-tests'
    '--enable-gcov --disable-gcov'
    '--enable-gmock --disable-gmock'
)

plank_pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

plank_pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

